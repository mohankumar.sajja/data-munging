from weather import WeatherSpread
from football import GoalsDifference

def call_file(cls, filename, index, index_one, index_two):
    obj = cls()
    data = obj.get_data(filename, index, index_one, index_two)
    if cls is WeatherSpread:
        print("Day: {}\nSpread: {}".format(data[0], data[1]))
    if cls is GoalsDifference:
        print("$Team Name: {}\nGoals Difference: {}".format(data[0], data[1]))
    

call_file(WeatherSpread, 'weather.dat', 0, 1, 2)
call_file(GoalsDifference, 'football.dat', 1, 6, 8)