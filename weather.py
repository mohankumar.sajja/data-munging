"""
Weather python file
"""
from data_munging import DataMunging

class WeatherSpread(DataMunging):
    """ class module for get data from weather.dat """
    def __init__(self):
        self.day = ''
        super().__init__()

    def get_data(self, filename, index, index_one, index_two):
        """ This method returns team and goals difference """
        return self.calculation(filename, index, index_one, index_two)

