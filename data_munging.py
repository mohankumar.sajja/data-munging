"""
Data munging file
"""
import math

class DataMunging:
    """
    Class for read data from given file
    """
    def __init__(self):
        self.difference = math.inf
        self.file_data = []

    def open_file(self, filename):
        """
        Function for extract data from given file
        """
        with open(filename, 'r') as file:
            line = file.readline()
            while line != '':
                line = file.readline()
                line_data = line.split()
                if line_data:
                    self.file_data.append(line_data)
        return self.file_data

    def calculation(self, filename, index, index_one, index_two):
        """ Do calculation part for difference """
        self.open_file(filename)
        for data in self.file_data:
            if data and '-' not in data[0]:
                if '*' in data[index_one] or '*' in data[index_two]:
                    data[index_one] = data[index_one].replace('*', '')
                    data[index_two] = data[index_two].replace('*', '')

                spread = abs(float(data[index_one])-float(data[index_two]))
                if spread < self.difference:
                    self.difference = spread
                    self.column = data[index]
        return self.column, self.difference
